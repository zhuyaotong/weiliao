package pers.tong.weiliao.model.po;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@AllArgsConstructor
@Data
public class GroupInfo {

    private String groupId;
    private String groupName;
    private String groupAvatarUrl;
    private List<UserInfo> members;

}
