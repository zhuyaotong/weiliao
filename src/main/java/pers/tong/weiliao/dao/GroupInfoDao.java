package pers.tong.weiliao.dao;


import pers.tong.weiliao.model.po.GroupInfo;

public interface GroupInfoDao {

    void loadGroupInfo();

    GroupInfo getByGroupId(String groupId);
}
