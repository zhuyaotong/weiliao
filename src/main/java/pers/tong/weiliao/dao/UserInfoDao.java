package pers.tong.weiliao.dao;

import pers.tong.weiliao.model.po.UserInfo;

public interface UserInfoDao {

    void loadUserInfo();

    UserInfo getByUsername(String username);

    UserInfo getByUserId(String userId);
}
