package pers.tong.weiliao.service;

import org.springframework.web.multipart.MultipartFile;
import pers.tong.weiliao.model.vo.ResponseJson;

import javax.servlet.http.HttpServletRequest;

public interface FileUploadService {

    ResponseJson upload(MultipartFile file, HttpServletRequest request);

}
