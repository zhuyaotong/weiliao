package pers.tong.weiliao.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pers.tong.weiliao.dao.UserInfoDao;
import pers.tong.weiliao.model.po.UserInfo;
import pers.tong.weiliao.model.vo.ResponseJson;
import pers.tong.weiliao.service.UserInfoService;

@Service
public class UserInfoServiceImpl implements UserInfoService {

    @Autowired
    private UserInfoDao userInfoDao;

    @Override
    public ResponseJson getByUserId(String userId) {
        UserInfo userInfo = userInfoDao.getByUserId(userId);
        return new ResponseJson().success().setData("userInfo", userInfo);
    }
}
