package pers.tong.weiliao.service;

import pers.tong.weiliao.model.vo.ResponseJson;

public interface UserInfoService {

    ResponseJson getByUserId(String userId);

}
