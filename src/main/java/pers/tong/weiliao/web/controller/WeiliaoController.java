package pers.tong.weiliao.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import pers.tong.weiliao.model.vo.ResponseJson;
import pers.tong.weiliao.service.UserInfoService;
import pers.tong.weiliao.util.Constant;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/weiliao")
public class WeiliaoController {

    @Autowired
    UserInfoService userInfoService;

    /**
     * 描述：登录成功后，调用此接口进行页面跳转
     *
     * @return
     */
    @GetMapping
    public String toWeiliao() {
        return "weiliao";
    }

    /**
     * 描述：登录成功跳转页面后，调用此接口获取用户信息
     *
     * @return
     */
    @PostMapping("/get_userinfo")
    @ResponseBody
    public ResponseJson getUserInfo(HttpSession session) {
        Object userId = session.getAttribute(Constant.USER_TOKEN);
        return userInfoService.getByUserId((String) userId);
    }
}
