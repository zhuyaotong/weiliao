package pers.tong.weiliao.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import pers.tong.weiliao.model.vo.ResponseJson;
import pers.tong.weiliao.service.FileUploadService;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/weiliao")
public class FileUploadController {

    @Autowired
    private FileUploadService fileUploadService;

    @PostMapping(value = "/upload")
    @ResponseBody
    public ResponseJson upload(
            @RequestParam(value = "file", required = true) MultipartFile file,
                               HttpServletRequest request) {

        return fileUploadService.upload(file, request);
    }

}
