package pers.tong.weiliao.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import pers.tong.weiliao.model.vo.ResponseJson;
import pers.tong.weiliao.service.SecurityService;

import javax.servlet.http.HttpSession;

@Controller
public class SecurityController {

    @Autowired
    SecurityService securityService;

    @GetMapping(value = {"login", "/"})
    public String toLogin() {
        return "login";
    }

    @PostMapping("login")
    @ResponseBody
    public ResponseJson login(HttpSession session,
                              @RequestParam String username,
                              @RequestParam String password) {
        return securityService.login(username, password, session);
    }

    @PostMapping("logout")
    @ResponseBody
    public ResponseJson logout(HttpSession session) {
        return securityService.logout(session);
    }

}
